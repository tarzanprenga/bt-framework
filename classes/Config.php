<?php 
class Config{
	public static function get($path = null){
		if($path){
			$config = $GLOBALS['config'];
			$path = explode('/',$path);

			foreach ($path as $sub) {
				if(isset($config[$sub])){
					$config = $config[$sub];
				}
			}
			return $config;
		}
		return false;
	}

	public static function path($append){
		return 'http://'. $_SERVER['SERVER_NAME'] . $append;
	}

}